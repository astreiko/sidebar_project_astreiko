import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'header-app',
  templateUrl: './header-app.component.html',
  styleUrls: ['./header-app.component.scss'],
})
export class HeaderAppComponent {
  @Input() public isReduceSidebar: boolean = false;

  @Output() public openNotifyModal:EventEmitter<void> = new EventEmitter<void>();
  @Output() public mainUserChoice: EventEmitter<void> = new EventEmitter<void>();
  @Output() public setLocalStorage: EventEmitter<void> = new EventEmitter<void>();

  constructor(
  ) {}
}
