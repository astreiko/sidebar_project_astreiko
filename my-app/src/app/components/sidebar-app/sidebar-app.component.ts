import { Component, Input } from '@angular/core';

import { Sidebar } from '../../shared/models/interfaces';
import { SIDEBAR_ITEMS } from 'src/app/shared/constants/sidebar-items';

@Component({
  selector: 'sidebar-app',
  templateUrl: './sidebar-app.component.html',
  styleUrls: ['./sidebar-app.component.scss'],
})
export class SidebarAppComponent {
  @Input() public deleteSidebar: boolean = false;
  @Input() public reduceSidebar: boolean = false;
  @Input() public minimalSidebar: boolean = false;
  @Input() public userMakeChoice: boolean = false;

  public readonly SIDEBAR_ITEMS: Sidebar[] = SIDEBAR_ITEMS;
}
