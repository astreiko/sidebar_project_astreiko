import { Component, OnInit } from '@angular/core';

import { ProductDataTest } from './shared/models/url.models';
import { ModalService } from './shared/services/modal.service';
import { ProductService } from './shared/services/product.service';
import { SidebarService } from './shared/services/sidebar.service';
import { LocalStorageService } from './shared/services/local-storage.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public TEST = {"data":[{"name":"Face to Face","id":1,"createdById":"00000000-0000-0000-0000-000000000000","createdDateTime":"2022-04-12T23:14:10.3700721+00:00","updatedMemo":"Created by migration","updatedDateTime":"2022-04-12T23:14:10.3700721+00:00"},{"name":"Lab Result","id":2,"createdById":"00000000-0000-0000-0000-000000000000","createdDateTime":"2022-04-12T23:14:10.3700721+00:00","updatedMemo":"Created by migration","updatedDateTime":"2022-04-12T23:14:10.3700721+00:00"},{"name":"Sleep Study","id":6,"createdById":"be00fd17-47ca-4e5e-ad4d-0b1537d567e5","createdDateTime":"2022-04-12T00:00:00+03:00","updatedMemo":"created","updatedDateTime":"2022-04-12T00:00:00+03:00"}],"isOk":true};

  constructor(
    public modalService: ModalService,
    public sidebarService: SidebarService,
    public localStorageService: LocalStorageService,
    public productService: ProductService,
  ) {}

  public ngOnInit(): void {
    console.log(ProductDataTest.getDataFromApiTest(this.TEST))
    this.sidebarService.loadingStatusSidebar();
    this.sidebarService.getLocalStorage();

    if (this.localStorageService.getItem(this.sidebarService.REDUCE_SIDEBAR) === null) {
      this.sidebarService.loadingStatusSidebar();
    }
  }
}
