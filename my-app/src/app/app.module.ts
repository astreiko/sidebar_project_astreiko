import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card'

import { MatIconModule } from '@angular/material/icon';
import { MatRadioModule } from '@angular/material/radio';
import { MatGridListModule } from '@angular/material/grid-list';

import { AppComponent } from './app.component';
import { UsersComponent } from './pages/users/users';
import { MatTabsModule } from '@angular/material/tabs';
import { AppRoutingModule } from './app-routing.module';
import { SliderComponent } from './pages/slider/slider';
import { SHARED_COMPONENTS } from './shared/components';
import { MatButtonModule } from '@angular/material/button';
import { PatientComponent } from './pages/patient/patient';
import { UrlService } from './shared/services/url.service';
import { TableComponent } from './pages/table-app/table-app';
import { AccountsComponent } from './pages/accounts/accounts';
import { ProductsComponent } from './pages/products/products';
import { ModalService } from './shared/services/modal.service';
import { SuppliersComponent } from './pages/suppliers/suppliers';
import { SliderService } from './shared/services/slider.service';
import { RequestService } from './shared/services/request.service';
import { MyProfileComponent } from './pages/my profile/my-profile';
import { SidebarService } from './shared/services/sidebar.service';
import { ProductService } from './shared/services/product.service';
import { SpinnerService } from './shared/services/spinner.service';
import { NewRequestComponent } from './pages/new request/new-request';
import { MatToolbarModule } from  '@angular/material/toolbar';
import { LocalStorageService } from './shared/services/local-storage.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserManagementComponent } from './pages/user management/user-management';
import { HeaderAppComponent } from './components/header-app/header-app.component';
import { SidebarAppComponent } from './components/sidebar-app/sidebar-app.component';
import { WithoutRoutingMenuComponent } from './pages/without routing menu/without-routing-menu';
import { UserRegistrationFormComponent } from './shared/components/user-registration-form/user-registration-form.component';
import { MatSnackBarModule } from "@angular/material/snack-bar";

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    UsersComponent,
    SliderComponent,
    PatientComponent,
    SHARED_COMPONENTS,
    AccountsComponent,
    ProductsComponent,
    MyProfileComponent,
    SuppliersComponent,
    HeaderAppComponent,
    SidebarAppComponent,
    NewRequestComponent,
    UserManagementComponent,
    WithoutRoutingMenuComponent,
    UserRegistrationFormComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    MatTabsModule,
    MatButtonModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    MatToolbarModule,

    MatIconModule,


    MatRadioModule,
    MatGridListModule,
    MatSnackBarModule,
  ],
  providers: [
    UrlService,
    ModalService,
    SliderService,
    RequestService,
    SpinnerService,
    ProductService,
    SidebarService,
    LocalStorageService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
