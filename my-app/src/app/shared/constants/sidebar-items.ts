import { Sidebar } from '../models/interfaces';

export const SIDEBAR_ITEMS: Sidebar[] = [
  {
    text: 'New',
    link: '/new-request',
    svg: './assets/images/svg/list.svg',
  },
  {
    text: 'Patient',
    link: '/patient',
    svg: './assets/images/svg/desktop.svg',
  },
  {
    text: 'User',
    link: 'user-management',
    svg: './assets/images/svg/user.svg',
  },
  {
    text: 'Users',
    link: '/users',
    svg: './assets/images/svg/user.svg',
  },
  {
    text: 'Profile',
    link: '/my-profile',
    svg: './assets/images/svg/done.svg',
  },
  {
    text: 'Account Models',
    link: '/accounts',
    svg: './assets/images/svg/blocks.svg',
  },
  {
    text: 'Suppliers',
    link: '/suppliers',
    svg: './assets/images/svg/desktop.svg',
  },
  {
    text: 'Products',
    link: '/products',
    svg: './assets/images/svg/blocks.svg',
  },
  {
    text: 'Menu',
    link: '/without-routing-menu',
    svg: './assets/images/svg/slider.svg',
  },
  {
    text: 'Slider',
    link: '/slider',
    svg: './assets/images/svg/slider.svg',
  },
  {
    text: 'Table',
    link: '/table-app',
    svg: './assets/images/svg/table.svg',
  },
  {
    text: 'Edit Product',
    link: '/edit-product',
    svg: './assets/images/svg/slider.svg',
  },
  {
    text: 'User Registration',
    link: '/user-registration',
    svg: './assets/images/svg/user.svg',
  },
]
