import { Nav, Item } from 'src/app/shared/models/interfaces';

export const BUTTONSMENU: Nav[] = [
  {
    isActive: false,
    id: 1,
    icon: './assets/images/svg/user.svg',
    name: 'Account',
  },
  {
    isActive: false,
    id: 2,
    icon: './assets/images/svg/folder-password.svg',
    name: 'Password',
  },
  {
    isActive: false,
    id: 3,
    icon: './assets/images/svg/mobile-browser.svg',
    name: 'Mobile Number',
  },
  {
    isActive: false,
    id: 4,
    icon: './assets/images/svg/email.svg',
    name: 'Email',
  },
];

export const ITEMS: Item[] = [
  {
    name: 'Body 1',
    isDisplayPage: true,
    id: '1',
  },
  {
    name: 'Body 2',
    isDisplayPage: true,
    id: '2',
  },
  {
    name: 'Body 3',
    isDisplayPage: true,
    id: '3',
  },
  {
    name: 'Body 4',
    isDisplayPage: true,
    id: '4',
  },
];