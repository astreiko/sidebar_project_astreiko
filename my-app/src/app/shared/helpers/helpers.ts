import {publishFacade} from "@angular/compiler";

export function getRandomId(): string{
  let possible: string = '0123456789';
  let newId: string = '';

  for (let i = 0; i < 4; i++) {

    newId += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return newId;
}

// export function getRandomGender(): string {
//   let a = Math.floor(Math.random() * 2);
//   let b: string = '';
//   let c: string = '';
//
//   if (a === 0) {
//     return b = 'Man';
//   }
//   else return c = 'Woman';
// }

export function getGender(dataLength: number, itemIndex: number): string {
  if (itemIndex < dataLength/2) {
    return 'man'
  } else {
    return 'woman'
  }
}

