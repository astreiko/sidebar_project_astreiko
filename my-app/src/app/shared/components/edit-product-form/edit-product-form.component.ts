import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { ModalService } from "../../services/modal.service";
import { Product } from "../../models/url.models";
import { ProductService } from "../../services/product.service";
import { FormControl, FormGroup, NgForm, Validators} from '@angular/forms';


@Component({
  selector: 'edit-product',
  templateUrl: './edit-product-form.component.html',
  styleUrls: ['./edit-product-form.component.scss'],
})
export class EditProductFormComponent {
  @Input() public product: Product;

  @Output() saveEditModal: EventEmitter<Product> = new EventEmitter<Product>();

  public products: Product[] = [];

  constructor(
    public modalService: ModalService,
    public productService: ProductService,
  ) {}

}
