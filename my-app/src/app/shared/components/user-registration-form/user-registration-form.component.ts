import { Component } from '@angular/core';

@Component({
  selector: 'user-registration',
  templateUrl: './user-registration-form.component.html',
  styleUrls: ['./user-registration-form.component.scss'],
})
export class UserRegistrationFormComponent {
  public isSuccess: boolean = false;

  constructor(
  ) {}

  public register(): void {
    console.log('Everything is OK')
    this.isSuccess = true;
    setTimeout(() => {
      this.isSuccess = !this.isSuccess;
    }, 3000);
  }
}
