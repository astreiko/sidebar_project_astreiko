import { Component, HostListener } from '@angular/core';

import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'popup-modal',
  templateUrl: './popup-modal.component.html',
  styleUrls: ['./popup-modal.component.scss'],
})
export class PopupModalComponent {

  constructor(
    public modalService: ModalService,
  ) {}

  @HostListener ('document:keydown', ['$event'])
  onKeyDownHandler (event: KeyboardEvent) {
    if (event.key === "Escape") {
      this.modalService.closeNotifyModal();
    }
  }
}