import { EditProductFormComponent } from "./edit-product-form/edit-product-form.component";
import { PopupModalComponent } from "./popup-modal/popup-modal.component";
import { SpinnerAppComponent } from "./spiner-app/spinner-app.component";

export const SHARED_COMPONENTS = [
  EditProductFormComponent,
  PopupModalComponent,
  SpinnerAppComponent,
]
