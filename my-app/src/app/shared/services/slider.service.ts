import { Injectable } from '@angular/core';

import { ARROWS } from '../constants/slider-constants';

@Injectable()
export class SliderService {
  public id: number = 0;
  public popup: boolean = false;

  public showText(): string {
    return ARROWS [this.id];
  }

  public rightText(): void {
    if (this.id < ARROWS.length - 1) {
      this.id ++;
    } else {
      this.id = 0;
    }
  }

  public leftText(): void {
    if (this.id > 0) {
      this.id --;
    } else {
      this.id = ARROWS.length - 1;
    }
  }
}
