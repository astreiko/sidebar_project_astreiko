import { Injectable } from "@angular/core";

@Injectable()
export class ModalService {
  public popup: boolean = false;
  public edit: boolean = false;

  public openNotifyModal(): void {
    this.popup = true;
  }

  public closeNotifyModal(): void {
    this.popup = false;
  }

  public closeEditModal(): void {
    this.edit = false;
  }
}
