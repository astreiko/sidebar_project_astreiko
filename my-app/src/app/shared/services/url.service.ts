import { Injectable } from '@angular/core';

@Injectable()
export class UrlService {
  public getProductURL(category: string): string {
    return 'https://api.publicapis.org/entries?' +
      (category ? 'category=' + category : '')
  }
}
