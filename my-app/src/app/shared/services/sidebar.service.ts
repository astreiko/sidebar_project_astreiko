import { Injectable } from '@angular/core';

import { LocalStorageService } from './local-storage.service';

@Injectable()
export class SidebarService {
  public isReduceSidebar: boolean = false;
  public isDeleteSidebar: boolean = false;
  public isUserMakeChoice: boolean = false;
  public isMinimalSidebar: boolean = false;

  public readonly REDUCE_SIDEBAR: string = 'reduceSidebarApp';

  private readonly REDUCE_WIDTH: number = 1000;
  private readonly DELETE_WIDTH: number = 600;

  constructor(
    public localStorageService: LocalStorageService,
  ) { }

  public setLocalStorage(): void {
    if (window.innerWidth > this.DELETE_WIDTH) {
      this.localStorageService.setItem(this.REDUCE_SIDEBAR, this.isReduceSidebar);
    }
  }

  public getLocalStorage(): void {
    this.isReduceSidebar = this.localStorageService.getItem(this.REDUCE_SIDEBAR);
  }

  public mainUserChoice(): void {
    if (window.innerWidth <= this.DELETE_WIDTH) {
      this.isReduceSidebar = false;
      this.isDeleteSidebar = !this.isDeleteSidebar;
      this.isMinimalSidebar = !this.isMinimalSidebar;
    }

    if (window.innerWidth > this.DELETE_WIDTH) {
      this.isUserMakeChoice = true;
      this.isDeleteSidebar = false;
      this.isReduceSidebar = !this.isReduceSidebar;
      this.isMinimalSidebar = false;
    }
  }

  public windowReduce(): void {
    this.isDeleteSidebar = SidebarService.getSidebarReduce(this.DELETE_WIDTH);

    if (window.innerWidth <= this.DELETE_WIDTH) {
      this.isMinimalSidebar = false;
    }

    if (window.innerWidth > this.DELETE_WIDTH) {
      if (this.localStorageService.getItem(this.REDUCE_SIDEBAR) !== null) {
        this.isReduceSidebar = this.localStorageService.getItem(this.REDUCE_SIDEBAR);
      } else {
        this.isReduceSidebar = SidebarService.getSidebarReduce(this.REDUCE_WIDTH);
      }
    }
  }

  public loadingStatusSidebar(): void {
    if (window.innerWidth > this.DELETE_WIDTH) {
      this.isReduceSidebar = true;
    }

    if (window.innerWidth > this.REDUCE_WIDTH) {
      this.isReduceSidebar = false;
    }

    if (window.innerWidth <= this.DELETE_WIDTH) {
      this.isDeleteSidebar = true;
      this.isMinimalSidebar = false;
      this.isUserMakeChoice = false;
    }
  }

  private static getSidebarReduce(width: number): boolean {
    return window.innerWidth <= width;
  }
}
