import { Injectable } from "@angular/core";

@Injectable()
export class LocalStorageService {

  public setItem(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }

  public getItem(key: string): boolean {
    const isReduceSidebar = localStorage.getItem(key);
    return (isReduceSidebar) ? JSON.parse(isReduceSidebar) : null;
  }
}
