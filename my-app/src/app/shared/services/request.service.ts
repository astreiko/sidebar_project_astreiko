import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable()
export class RequestService {

  constructor(
    private http: HttpClient,
  ) { }

  getProduct(url: string): any {
    return this.http.get(url);
  }
}
