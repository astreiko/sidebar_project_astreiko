import { Injectable } from '@angular/core';

import { map, Observable } from 'rxjs';
import { UrlService } from './url.service';
import { RequestService } from './request.service';
import { ProductDataApi, ProductData, Product } from '../models/url.models';

@Injectable()
export class ProductService {

  constructor(
    private httpService: RequestService,
    private urlService: UrlService,
  ) {}

   public getProduct$(category: string): Observable<ProductData> {
     return this.httpService.getProduct(this.urlService.getProductURL(category))
       .pipe(map((data: ProductDataApi): ProductData => ProductData.getDataFromApiModel(data)))
   }

  public deleteProducts$(category: string): Observable<void> {
    return this.httpService.getProduct(this.urlService.getProductURL(category))
  }
}
