import { getRandomId, getGender } from '../helpers/helpers';

export class ProductDataApi {
  count: number;
  entries: ProductApi[];
}

export class ProductDataApiTest {
  data: ProductApiTest[];
  isOk: boolean;
}

export class ProductApi {
  API: string;
  Auth: string;
  Category: string;
  Cors: string;
  Description: string;
  HTTPS: boolean;
  Link: string;

  public static getGender(dataLength: number, itemIndex: number): string {
    if (itemIndex < dataLength/2) {
      return 'man'
    } else {
      return 'woman'
    }
  }
}

export class ProductApiTest {
  createdById: string;
  createdDateTime: string;
  id: number;
  name: string;
  updatedDateTime: string;
  updatedMemo: string;
}

export class ProductData {
  count: number;
  data: Product[];

  public static getDataFromApiModel(data: ProductDataApi): ProductData {
    return {
      count: data.count,
      data: data.entries.map((it, itemData) => Product.getDataFromApiModel(it, data.entries.length, itemData)),
    }
  }
}

export class ProductDataTest {
  isOkTest: boolean;
  dataTest: ProductTest[];

  public static getDataFromApiTest(dataTest: ProductDataApiTest): ProductDataTest {
    return {
      isOkTest: dataTest.isOk,
      dataTest: dataTest.data.map((it) => ProductTest.getDataFromApiTest(it)),
    }
  }
}

export class Product {
  api: string;
  auth: string;
  category: string;
  cors: string;
  description: string;
  https: boolean;
  link: string;

  //FE only
  id: string;
  gender: string;

  public static getDataFromApiModel(data: ProductApi, dataLength: number, itemIndex: number): Product {
    return {
      id: getRandomId(),
      gender: getGender(dataLength, itemIndex),
      api: data.API,
      auth: data.Auth,
      category: data.Category,
      cors: data.Cors,
      description: data.Description,
      https: data.HTTPS,
      link: data.Link,
    }
  }
}

export class ProductTest {
  byId: string;
  dateTime: string;
  id: number;
  name: string;
  updatedTime: string;
  nemo: string;

  public static getDataFromApiTest(dataTest: ProductApiTest): ProductTest {
    return {
      byId: dataTest.createdById,
      dateTime: dataTest.createdDateTime,
      id: dataTest.id,
      name: dataTest.name,
      updatedTime: dataTest.updatedDateTime,
      nemo: dataTest.updatedMemo,
    }
  }
}
