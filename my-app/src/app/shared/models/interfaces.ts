export class Sidebar {
  text: string;
  link: string;
  svg: string;
}

export class Nav {
  icon: string;
  name: string;
  id: number;
  isActive: boolean;
}

export class Products {
  name: string;
  hcpc: number;
  id: number;
}

export class Item {
  name: string;
  isDisplayPage: boolean;
  id: string;
}


