import { Component, OnInit } from '@angular/core';

import { BUTTONSMENU, ITEMS } from 'src/app/shared/constants/without-routing-menu-items';

@Component({
  selector: 'without-routing-menu',
  templateUrl: './without-routing-menu.html',
  styleUrls: ['./without-routing-menu.scss'],
})
export class WithoutRoutingMenuComponent implements OnInit {
  public ITEMS = ITEMS;
  public BUTTONS = BUTTONSMENU;
  public isDisplayPage: boolean = true;
  public buttonIdActiveDefault: number = 1;
  public isShowMenuAccount: boolean = false;

  public ngOnInit(): void {
    this.setActiveTabDefault();
  }

  public setActiveTabDefault(): void {
    this.isShowMenuAccount = true;
    BUTTONSMENU[0].isActive = true;
  }

  public changeItem(bittonId: number): void {
    this.setActiveTab(bittonId);
    this.buttonIdActiveDefault = bittonId;
  }

  private setActiveTab(tabId: number): void {
    BUTTONSMENU.forEach((button) => {

      return button.id === tabId ? button.isActive = true : button.isActive = false;
    });
  }
}
