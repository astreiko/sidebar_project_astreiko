import { Component, Output, OnInit, EventEmitter } from '@angular/core';

import { Product } from '../../shared/models/url.models';
import { MALE, FEMALE } from '../../shared/constants/genders';
import { ProductService } from '../../shared/services/product.service';
import { SpinnerService } from '../../shared/services/spinner.service';
import { ModalService } from "../../shared/services/modal.service";


@Component({
  selector: 'table-app',
  templateUrl: './table-app.html',
  styleUrls: ['./table-app.scss'],
})
export class TableComponent implements OnInit {

  public product: Product[] = [];
  public categoryMenu: string[] = [];
  public genderMenu: string[] = [];
  public categories: null = null;
  public genders: null = null;
  public male: string = 'woman'
  public maleSort: Product[] = [];
  public selectProduct: Product;

  private sorting: boolean = true;

  constructor(
    public productService: ProductService,
    public spinnerService: SpinnerService,
    public modalService: ModalService,
  ) {
  }

  public ngOnInit(): void {
  }

  public editProduct(item: Product): void {
    this.modalService.edit = true;
    this.selectProduct = item;
    console.log(this.selectProduct.cors);
  }

  public getAllProducts(product: Product[]): void {
    this.spinnerService.spinner = true;

    setTimeout(() => {
      this.spinnerService.spinner = false;
    }, 1000);

    this.productService
      .getProduct$('')
      .subscribe(data => {
        this.product = data.data;
        this.getFilterCategory(data?.data);
      });
  }

  public getFilterCategory(data: Product[]): void {
    let categoryArray: string[] = [];
    let genderArray: string[] = [];
    data.forEach(cat => categoryArray.push(cat.category));
    data.forEach(gen => genderArray.push(gen.gender));
    this.categoryMenu = Array.from(new Set(categoryArray));
    console.log(this.categoryMenu);
    this.genderMenu = Array.from(new Set(genderArray));
    console.log(this.genderMenu);
  }

  public getGenders(): void {
    if (this.male === MALE) {
      this.male = FEMALE;
    } else {
      this.male = MALE;
    }

    if (!this.maleSort.length) {
      this.maleSort = this.product
    }

    this.product = this.maleSort.filter(gender => {
        return gender.gender === this.male;
      });
  }

  public getCategory(category: string): void {
    this.productService
      .getProduct$(category)
      .subscribe(val => this.product = val?.data);
  }

  public deleteAllProduct(): void {
    this.product = [];
  }

  public deleteProduct(category: string): void {
    this.product = this.product.filter(it => it.id !== category);
  }

  public sortProduct(str: string): void {
    this.sortByItem(this.product, str);
  }

  public sortByItem(criteria: Product[], item: string): Product[] {
    criteria.sort((first: any, next: any) => {
      if (this.sorting) {
        if (first[item].toLowerCase() > next[item].toLowerCase()) {
          return -1;
        } else if (first[item].toLowerCase() < next[item].toLowerCase()) {
          return 1;
        }
        return 1;
      } else {
        return -1;
      }
    });
    this.sorting = !this.sorting;

    return criteria;
  }

  public saveEditModal(form: Product): void {
    let ind = this.product.findIndex(product => product.id === this.selectProduct.id);

    if (ind !== -1) {
      this.product[ind].api = form.api;
      this.product[ind].auth = form.auth;
      this.product[ind].cors = form.cors;
      console.log(this.product[ind].cors);
    }

    console.log(this.selectProduct.id)
    console.log(form);
    console.log(this.product)
    console.log(form.cors)

    this.modalService.edit = false;
  }

}
