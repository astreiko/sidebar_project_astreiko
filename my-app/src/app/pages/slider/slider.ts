import { Component } from '@angular/core';

import { ModalService } from '../../shared/services/modal.service';
import { SliderService } from '../../shared/services/slider.service';

@Component({
  selector: 'slider',
  templateUrl: './slider.html',
  styleUrls: ['./slider.scss'],
})
export class SliderComponent {

  constructor(
    public modalService: ModalService,
    public sliderService: SliderService,
  ) {}
}