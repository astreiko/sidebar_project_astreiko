import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsersComponent } from './pages/users/users';
import { SliderComponent } from './pages/slider/slider';
import { PatientComponent } from './pages/patient/patient';
import { TableComponent } from './pages/table-app/table-app';
import { ProductsComponent } from './pages/products/products';
import { AccountsComponent } from './pages/accounts/accounts';
import { SuppliersComponent } from './pages/suppliers/suppliers';
import { MyProfileComponent } from './pages/my profile/my-profile';
import { NewRequestComponent } from './pages/new request/new-request';
import { UserManagementComponent } from './pages/user management/user-management';
import { PopupModalComponent } from './shared/components/popup-modal/popup-modal.component';
import { WithoutRoutingMenuComponent } from './pages/without routing menu/without-routing-menu';
import {EditProductFormComponent} from "./shared/components/edit-product-form/edit-product-form.component";
import {
  UserRegistrationFormComponent
} from "./shared/components/user-registration-form/user-registration-form.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/slider',
    pathMatch: 'full',
  },
  {
    path: 'table-app',
    component: TableComponent,
  },
  {
    path: 'users',
    component: UsersComponent,
  },
  {
    path: 'slider',
    component: SliderComponent,
  },
  {
    path: 'patient',
    component: PatientComponent,
  },
  {
    path: 'products',
    component: ProductsComponent,
  },
  {
    path: 'accounts',
    component: AccountsComponent,
  },
  {
    path: 'suppliers',
    component: SuppliersComponent,
  },
  {
    path: 'my-profile',
    component: MyProfileComponent,
  },
  {
    path: 'new-request',
    component: NewRequestComponent,
  },
  {
    path: 'popup-modal',
    component: PopupModalComponent,
  },
  {
    path: 'user-management',
    component: UserManagementComponent,
  },
  {
    path: 'edit-product',
    component: EditProductFormComponent,
  },
  {
    path: 'without-routing-menu',
    component: WithoutRoutingMenuComponent,
  },
  {
    path: 'user-registration',
    component: UserRegistrationFormComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
