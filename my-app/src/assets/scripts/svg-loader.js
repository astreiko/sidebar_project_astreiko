(() => {
    "use strict";
    var e = {
        701: e => {
            let t = 0;
            e.exports = {incr: () => ++t, decr: () => --t, curr: () => t}
        }, 941: e => {
            e.exports = (e, t, r = "") => {
                const n = /url\(['"]?#([\w:.-]+)['"]?\)/g, o = /#([\w:.-]+)/g;
                return t.match(n) && (t = t.replace(n, (function (t, r) {
                    return e[r] ? `url(#${e[r]})` : t
                }))), ["href", "xlink:href"].includes(r) && t.match(o) && (t = t.replace(o, (function (t, r) {
                    return e[r] ? `#${e[r]}` : t
                }))), t
            }
        }, 905: e => {
            e.exports = (e, t, r) => {
                const n = new RegExp("([^\r\n,{}]+)(,(?=[^}]*{)|s*{)", "g");
                return e.replace(n, (function (e, n, o) {
                    if (n.match(/^\s*(@media|@.*keyframes|to|from|@font-face|1?[0-9]?[0-9])/)) return n + o;
                    const a = n.match(/#(\w+)/);
                    return a && r[a[1]] && (n = n.replace(a[0], `#${r[a[1]]}`)), (n = n.replace(/^(\s*)/, "$1" + t + " ")) + o
                }))
            }
        }, 175: (e, t, r) => {
            r.r(t), r.d(t, {Store: () => n, clear: () => u, del: () => c, get: () => s, keys: () => d, set: () => i});

            class n {
                constructor(e = "keyval-store", t = "keyval") {
                    this.storeName = t, this._dbp = new Promise(((r, n) => {
                        const o = indexedDB.open(e, 1);
                        o.onerror = () => n(o.error), o.onsuccess = () => r(o.result), o.onupgradeneeded = () => {
                            o.result.createObjectStore(t)
                        }
                    }))
                }

                _withIDBStore(e, t) {
                    return this._dbp.then((r => new Promise(((n, o) => {
                        const a = r.transaction(this.storeName, e);
                        a.oncomplete = () => n(), a.onabort = a.onerror = () => o(a.error), t(a.objectStore(this.storeName))
                    }))))
                }
            }

            let o;

            function a() {
                return o || (o = new n), o
            }

            function s(e, t = a()) {
                let r;
                return t._withIDBStore("readonly", (t => {
                    r = t.get(e)
                })).then((() => r.result))
            }

            function i(e, t, r = a()) {
                return r._withIDBStore("readwrite", (r => {
                    r.put(t, e)
                }))
            }

            function c(e, t = a()) {
                return t._withIDBStore("readwrite", (t => {
                    t.delete(e)
                }))
            }

            function u(e = a()) {
                return e._withIDBStore("readwrite", (e => {
                    e.clear()
                }))
            }

            function d(e = a()) {
                const t = [];
                return e._withIDBStore("readonly", (e => {
                    (e.openKeyCursor || e.openCursor).call(e).onsuccess = function () {
                        this.result && (t.push(this.result.key), this.result.continue())
                    }
                })).then((() => t))
            }
        }
    }, t = {};

    function r(n) {
        var o = t[n];
        if (void 0 !== o) return o.exports;
        var a = t[n] = {exports: {}};
        return e[n](a, a.exports, r), a.exports
    }

    r.d = (e, t) => {
        for (var n in t) r.o(t, n) && !r.o(e, n) && Object.defineProperty(e, n, {enumerable: !0, get: t[n]})
    }, r.o = (e, t) => Object.prototype.hasOwnProperty.call(e, t), r.r = e => {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {value: "Module"}), Object.defineProperty(e, "__esModule", {value: !0})
    }, (() => {
        const {get: e, set: t, del: n} = r(175), o = r(905), a = r(941), s = r(701), i = [], c = {}, u = (e, t, r) => {
            const {enableJs: n, disableUniqueIds: u, disableCssScoping: d} = t,
                l = (new DOMParser).parseFromString(r, "text/html"), b = l.querySelector("svg"), f = (() => {
                    if (i.length) return i;
                    for (const e in document.head) e.startsWith("on") && i.push(e);
                    return i
                })(), h = c[e.getAttribute("data-id")] || new Set,
                g = e.getAttribute("data-id") || `svg-loader_${s.incr()}`, p = {};
            u || Array.from(l.querySelectorAll("[id]")).forEach((e => {
                const t = e.getAttribute("id"), r = `${t}_${s.incr()}`;
                e.setAttribute("id", r), p[t] = r
            })), Array.from(l.querySelectorAll("*")).forEach((e => {
                if ("script" === e.tagName) {
                    if (!n) return void e.remove();
                    {
                        const t = document.createElement("script");
                        t.innerHTML = e.innerHTML, document.body.appendChild(t)
                    }
                }
                for (let t = 0; t < e.attributes.length; t++) {
                    const {name: r, value: o} = e.attributes[t], s = a(p, o, r);
                    o !== s && e.setAttribute(r, s), !f.includes(r.toLowerCase()) || n ? ["href", "xlink:href"].includes(r) && o.startsWith("javascript") && !n && e.removeAttribute(r) : e.removeAttribute(r)
                }
                if ("style" === e.tagName && !d) {
                    let t = o(e.innerHTML, `[data-id="${g}"]`, p);
                    t = a(p, t), t !== e.innerHTML && (e.innerHTML = t)
                }
            }));
            for (let t = 0; t < b.attributes.length; t++) {
                const {name: r, value: n} = b.attributes[t];
                e.getAttribute(r) && !h.has(r) || (h.add(r), e.setAttribute(r, n))
            }
            c[g] = h, e.setAttribute("data-id", g), e.innerHTML = b.innerHTML
        }, d = {}, l = {}, b = async r => {
            const o = r.getAttribute("data-src"), a = r.getAttribute("data-cache"),
                s = "enabled" === r.getAttribute("data-js"), i = "disabled" === r.getAttribute("data-unique-ids"),
                c = "disabled" === r.getAttribute("data-css-scoping"), f = await (async t => {
                    try {
                        let r = await e(`loader_${t}`);
                        if (!r) return;
                        return r = JSON.parse(r), Date.now() < r.expiry ? r.data : void n(`loader_${t}`)
                    } catch (e) {
                        return
                    }
                })(o), h = "disabled" !== a, g = u.bind(self, r, {enableJs: s, disableUniqueIds: i, disableCssScoping: c});
            if (l[o] || h && f) {
                const e = l[o] || f;
                g(e)
            } else {
                if (d[o]) return void setTimeout((() => b(r)), 20);
                d[o] = !0, fetch(o).then((e => {
                    if (!e.ok) throw Error(`Request for '${o}' returned ${e.status} (${e.statusText})`);
                    return e.text()
                })).then((e => {
                    const r = e.toLowerCase().trim();
                    if (!r.startsWith("<svg") && !r.startsWith("<?xml")) throw Error(`Resource '${o}' returned an invalid SVG file`);
                    h && (async (e, r, n) => {
                        try {
                            const o = parseInt(n, 10);
                            await t(`loader_${e}`, JSON.stringify({
                                data: r,
                                expiry: Date.now() + (Number.isNaN(o) ? 864e5 : o)
                            }))
                        } catch (e) {
                            console.error(e)
                        }
                    })(o, e, a), l[o] = e, g(e)
                })).catch((e => {
                    console.error(e)
                })).finally((() => {
                    delete d[o]
                }))
            }
        };
        if (globalThis.IntersectionObserver) {
            const e = new IntersectionObserver((t => {
                t.forEach((t => {
                    t.isIntersecting && (b(t.target), e.unobserve(t.target))
                }))
            }), {rootMargin: "1200px"})
        }
        const f = [];

        function h() {
            Array.from(document.querySelectorAll("svg[data-src]:not([data-id])")).forEach((e => {
                -1 === f.indexOf(e) && (f.push(e), "lazy" === e.getAttribute("data-loading") ? (void 0).observe(e) : b(e))
            }))
        }

        let g = !1;
        if (globalThis.addEventListener) {
            const e = setInterval((() => {
                h()
            }), 100);
            globalThis.addEventListener("DOMContentLoaded", (() => {
                clearInterval(e), h(), g || (g = !0, new MutationObserver((e => {
                    e.some((e => Array.from(e.addedNodes).some((e => e.nodeType === Node.ELEMENT_NODE && (e.getAttribute("data-src") && !e.getAttribute("data-id") || e.querySelector("svg[data-src]:not([data-id])")))))) && h(), e.forEach((e => {
                        "attributes" === e.type && b(e.target)
                    }))
                })).observe(document.documentElement, {
                    attributeFilter: ["data-src"],
                    attributes: !0,
                    childList: !0,
                    subtree: !0
                }))
            }))
        }
    })()
})();